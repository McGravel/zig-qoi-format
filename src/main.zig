const std = @import("std");

const Magic = [_]u8{ 'q', 'o', 'i', 'f' };
const HeaderSize = 14;

const DecodeError = error{
    InvalidValue,
    // TODO(McGravel): Debugging error types, could delete later?
    InvalidChannels,
    InvalidColorspace,
};

const Channels = enum(u8) {
    rgb = 3,
    rgba = 4,
};

const Colorspace = enum(u8) {
    srgb_with_linear_alpha = 0,
    all_channels_linear = 1,
};

const Header = struct {
    magic: [4]u8,
    width: u32, // Big Endian
    height: u32, // Big Endian
    channels: Channels,
    colorspace: Colorspace,
};

const Pixel = packed struct {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
};

const ChunkTags = enum(u8) {
    rgb = 0xfe,
    rgba = 0xff,
    index = 0x00,
    diff = 0x40,
    luma = 0x80,
    run = 0xc0,
};

fn readHeader(in: std.fs.File) !Header {
    const reader = in.reader();

    return .{
        .magic = _: {
            var magic_arr: [4]u8 = undefined;
            inline for (0..4) |i| {
                magic_arr[i] = try reader.readByte();
            }
            break :_ magic_arr;
        },
        .width = try reader.readInt(u32, std.builtin.Endian.big),
        .height = try reader.readInt(u32, std.builtin.Endian.big),
        .channels = _: {
            const c = try reader.readByte();
            if (c != @intFromEnum(Channels.rgb) and c != @intFromEnum(Channels.rgba)) {
                return DecodeError.InvalidChannels;
            }
            break :_ @enumFromInt(c);
        },
        .colorspace = _: {
            const c = try reader.readByte();
            if (c != @intFromEnum(Colorspace.srgb_with_linear_alpha) and c != @intFromEnum(Colorspace.all_channels_linear)) {
                return DecodeError.InvalidColorspace;
            }
            break :_ @enumFromInt(c);
        },
    };
}

fn indexHash(in: Pixel) u8 {
    const r: u16 = in.r;
    const g: u16 = in.g;
    const b: u16 = in.b;
    const a: u16 = in.a;
    const amt: u16 = ((r * 3) + (g * 5) + (b * 7) + (a * 11)) % 64;
    return @as(u8, @intCast(amt));
}

fn debiasDiffChunk(last_pixel: u8, diff: u8) u8 {
    switch (diff) {
        0 => return last_pixel -% 2,
        1 => return last_pixel -% 1,
        2 => return last_pixel,
        3 => return last_pixel +% 1,
        else => unreachable,
    }
}

fn decode(in: std.fs.File, header: Header, allocator: std.mem.Allocator) !void {
    const reader = in.reader();
    const two_bit_mask = 0xC0;
    try in.seekTo(HeaderSize);
    const total_pixel_amount: u64 = header.width * header.height;
    var out_pixels = try std.ArrayList(Pixel).initCapacity(allocator, total_pixel_amount);
    std.debug.print("Allocated: {d} Pixel objects\n", .{out_pixels.capacity});
    var last_pixel = Pixel{ .r = 0, .g = 0, .b = 0, .a = 255 };
    var pixel_history: [64]Pixel = .{Pixel{ .r = 0, .g = 0, .b = 0, .a = 0 }} ** 64;

    while (out_pixels.items.len < total_pixel_amount) {
        const chunk = try reader.readByte();

        if (chunk == @intFromEnum(ChunkTags.rgb)) {
            const rgb_pixel = Pixel{
                .r = try reader.readByte(),
                .g = try reader.readByte(),
                .b = try reader.readByte(),
                .a = last_pixel.a,
            };
            pixel_history[indexHash(rgb_pixel)] = rgb_pixel;
            out_pixels.appendAssumeCapacity(rgb_pixel);
            last_pixel = rgb_pixel;
        } else if (chunk == @intFromEnum(ChunkTags.rgba)) {
            const rgba_pixel = Pixel{
                .r = try reader.readByte(),
                .g = try reader.readByte(),
                .b = try reader.readByte(),
                .a = try reader.readByte(),
            };
            pixel_history[indexHash(rgba_pixel)] = rgba_pixel;
            out_pixels.appendAssumeCapacity(rgba_pixel);
            last_pixel = rgba_pixel;
        } else if (chunk & two_bit_mask == @intFromEnum(ChunkTags.index)) {
            const index: u8 = @intCast(chunk & 0b00111111);
            const indexed_pixel = pixel_history[index];
            out_pixels.appendAssumeCapacity(indexed_pixel);
            last_pixel = indexed_pixel;
        } else if (chunk & two_bit_mask == @intFromEnum(ChunkTags.diff)) {
            std.debug.print("\nDIFF", .{});
            const dr: u8 = @intCast((chunk & 0b00110000) >> 4);
            const dg: u8 = @intCast((chunk & 0b00001100) >> 2);
            const db: u8 = @intCast((chunk & 0b00000011));
            std.debug.print("\tdr {d}\tdg {d}\tdb {d}", .{ dr, dg, db });
            const diff_pixel = Pixel{
                .r = last_pixel.r +% dr -% 2,
                .g = last_pixel.g +% dg -% 2,
                .b = last_pixel.b +% db -% 2,
                .a = last_pixel.a,
            };
            std.debug.print("\nResulting Pixel {any}", .{diff_pixel});
            out_pixels.appendAssumeCapacity(diff_pixel);
            pixel_history[indexHash(diff_pixel)] = diff_pixel;
            last_pixel = diff_pixel;
        } else if (chunk & two_bit_mask == @intFromEnum(ChunkTags.luma)) {
            std.debug.print("LUMA\n", .{});
            const diff_green = chunk & 0b00111111;
            const diff_r_b = try reader.readByte();
            const diff_red: u8 = (diff_r_b & 0b11110000) >> 4;
            const diff_blue: u8 = diff_r_b & 0b00001111;
            const luma_pixel = Pixel{
                .r = last_pixel.r +% diff_green -% 32 +% diff_red -% 8,
                .g = last_pixel.g +% diff_green -% 32,
                .b = last_pixel.b +% diff_green -% 32 +% diff_blue -% 8,
                .a = last_pixel.a,
            };
            out_pixels.appendAssumeCapacity(luma_pixel);
            pixel_history[indexHash(luma_pixel)] = luma_pixel;
            last_pixel = luma_pixel;
        } else if (chunk & two_bit_mask == @intFromEnum(ChunkTags.run)) {
            // Take the inverse mask of the chunk as a u8 value to get the chunk's value
            //  and compensate for the bias as mentioned in the spec for "QOI_OP_RUN"
            const run_amount: u8 = (chunk & (~@as(u8, two_bit_mask))) + 1;
            std.debug.print("\nAppend {any} {d} times", .{ last_pixel, run_amount });
            out_pixels.appendNTimesAssumeCapacity(last_pixel, run_amount);
        }
    }
}

pub fn main() !void {}

test "basic file load" {
    const in_file = try std.fs.Dir.openFile(std.fs.cwd(), "qoi_test_images/dice.qoi", .{ .mode = .read_only });
    _ = try readHeader(in_file);
}
